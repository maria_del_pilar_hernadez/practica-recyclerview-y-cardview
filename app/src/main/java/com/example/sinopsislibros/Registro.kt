package com.example.sinopsislibros

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.sinopsislibros.SinopsisLibros.Companion.pref
import kotlinx.android.synthetic.main.activity_registro.*

class Registro : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        registra.setOnClickListener{
            val nom = nombre.text.toString()
            val apell = apellidos.text.toString()
            val correo = correo.text.toString()
            val con = contraseña.text.toString()
            val confirm = contraConfirm.text.toString()

            if(nom.isEmpty() || apell.isEmpty() || correo.isEmpty() || con.isEmpty() || confirm.isEmpty()){
                Toast.makeText(applicationContext, "Llene el formulario", Toast.LENGTH_LONG).show()
            }

            else if(con != confirm) {
                Toast.makeText(
                    applicationContext,
                    "Las Contraseñas no son iguales",
                    Toast.LENGTH_LONG
                ).show()
            }

            else {

                pref.saveName(nombre.text.toString())
                pref.saveContra(contraseña.text.toString())
                Toast.makeText(applicationContext, "Registro Exitoso" , Toast.LENGTH_LONG).show()
            }

        }
    }
}