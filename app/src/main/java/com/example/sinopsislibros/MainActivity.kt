package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.sinopsislibros.SinopsisLibros.Companion.pref
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciar.setOnClickListener {

            val usuario = usuario.text.toString()
            val contraseña = contra.text.toString()
            val name = pref.getName()
            val contraa = pref.getContra()

            if (usuario.isEmpty()) {
                Toast.makeText(this, "Ingresa un usuario", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

           else if (contraseña.isEmpty()) {
                Toast.makeText(this, "Ingrese una Contraseña", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            else if(name.equals(usuario) && contraa.equals(contraseña)){


                Toast.makeText(this, "Acceso Permitido", Toast.LENGTH_LONG).show()
                val intent = Intent(this,Generos::class.java)
                startActivity(intent)

            } else{

               Toast.makeText(this, "Debe Registrarse", Toast.LENGTH_LONG).show()
            }

        }

        datos.setOnClickListener {

            val intent = Intent(this,Registro::class.java)
            startActivity(intent)

        }
    }
}