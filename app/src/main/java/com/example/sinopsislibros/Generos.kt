package com.example.sinopsislibros

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class Generos : AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador:AdaptadorCustom? = null
    var layoutManager: RecyclerView.LayoutManager? = null   //el diseño

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generos)

        val platillos = ArrayList<ListaCategoria>()

        platillos.add(ListaCategoria("Terror" , R.drawable.c_terror))
        platillos.add(ListaCategoria("Clásicos" , R.drawable.clasicos_l))
        platillos.add(ListaCategoria("Romance" , R.drawable.corazon))
        platillos.add(ListaCategoria("Distopías" , R.drawable.c_disto))
        platillos.add(ListaCategoria("Fantasía" , R.drawable.fantasiaelfo))
        platillos.add(ListaCategoria("Ciencia Ficción" , R.drawable.cienciaficcion))
        platillos.add(ListaCategoria("Contemporaneós" , R.drawable.comtemporane))



        lista = findViewById(R.id.listaR)
        lista?.setHasFixedSize(true)  //adaptador tamaño de la vista

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager  // donde se dibuje el layout

        adaptador = AdaptadorCustom(this,platillos, object: ClickListener{
            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, "Cick", Toast.LENGTH_LONG).show()
            }
        })

        adaptador = AdaptadorCustom(this,platillos, object: ClickListener{

            override fun onClick(vista: View, index: Int) {
                Toast.makeText(applicationContext, platillos.get(index).titulo, Toast.LENGTH_LONG).show()



                if(index == 0){

                    startActivity(Intent(applicationContext, Lista_Libros::class.java))
                }
                else if(index == 1){
                    startActivity(Intent(applicationContext, Libro_Clasicos::class.java))
                }
                else if(index == 2){
                    startActivity(Intent(applicationContext, Libro_romance::class.java))
                }
                else if(index == 3){
                    startActivity(Intent(applicationContext, Libro_distopia::class.java))
                }
                else if(index == 4){
                    startActivity(Intent(applicationContext, Libro_fantasia::class.java))
                }
                else if(index == 5){
                    startActivity(Intent(applicationContext, Libro_ficcion::class.java))
                }
                else if(index == 6){
                    startActivity(Intent(applicationContext, Libro_contemporaneo::class.java))
                }

            }
        })

        lista?.adapter = adaptador

    }
}