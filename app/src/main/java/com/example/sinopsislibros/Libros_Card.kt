package com.example.sinopsislibros

class Libros_Card(nombre: String, imagen: Int, descripcion: String) {

    var nombre = ""
    var imagen = 0
    var descripcion = ""

    init {
        this.nombre = nombre
        this.imagen = imagen
        this.descripcion = descripcion
    }
}